terraform {
  backend "s3" {
    bucket = "base-config-341975"
    key    = "gitlab-runner-fleet-001"
    region = "us-east-1"
  }
}
